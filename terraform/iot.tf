# aws_iot_certificate cert
resource "aws_iot_certificate" "cert" {
  active = true
}

# aws_iot_policy pub-sub
resource "aws_iot_policy" "pubsub" {
  name = "PubySubToAnyTopic"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = file("files/iot_policy.json")
}

resource "aws_iot_policy_attachment" "att" {
  policy = aws_iot_policy.pubsub.name
  target = aws_iot_certificate.cert.arn
}
# aws_iot_thing temp_sensor
resource "aws_iot_thing" "example" {
  name = "example"

  attributes = {
    First = "examplevalue"
  }
}

# aws_iot_thing_principal_attachment thing_attachment
resource "aws_iot_thing_principal_attachment" "att" {
  principal = aws_iot_certificate.cert.arn
  thing     = aws_iot_thing.example.name
}
# data aws_iot_endpoint to retrieve the endpoint to call in simulation.py
data "aws_iot_endpoint" "example" {
  endpoint_type = "iot:Data-ATS"
}
# aws_iot_topic_rule rule for sending invalid data to DynamoDB
resource "aws_iot_topic_rule" "temperature_rule" {
  name        = "temperature_rule"
  sql         = "SELECT * FROM 'sensor/temperature/+' WHERE temperature >= 40"
  description = "Insert temperature data into DynamoDB when temperature is >= 40"
  enabled     = true
  sql_version = "2016-03-23"


  dynamodbv2 {
    role_arn   = aws_iam_role.iot_role.arn
    put_item {
      table_name            = "temperature-data"
    }


  }

}

resource "aws_iam_role" "iot_role" {
  name = "iot-dynamodb-role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Principal = {
          Service = "iot.amazonaws.com"
        }
      }
    ]
  })

  inline_policy {
    name = "dynamodb-policy"

    policy = jsonencode({
      Version = "2012-10-17"
      Statement = [
        {
          Action = [
            "dynamodb:PutItem"
          ]
          Effect = "Allow"
          Resource = "arn:aws:dynamodb:us-east-1:123456789012:table/temperature-data"
        }
      ]
    })
  }
}# aws_iot_topic_rule rule for sending valid data to Timestream
resource "aws_iot_topic_rule" "rule" {
  name      = "rule"
  sql       = "SELECT * FROM 'sensor/temperature/+'"
  enabled = true
  sql_version = "2016-03-23"

  timestream {
    role_arn         = "arn:aws:iam::123456789012:role/timestream-write-role"
    database_name    = "iot"
    table_name       = "temperaturesensor"
    dimension {
      name  = "sensor_id"
      value = "$${sensor_id}"
    }

    dimension {
      name  = "temperature"
      value = "$${temperature}"
    }

    dimension {
      name  = "zone_id"
      value = "$${zone_id}"
    }

    timestamp {
      unit  = "MILLISECONDS"
      value = "$${timestamp()}"
    }

  }
}




###########################################################################################
# Enable the following resource to enable logging for IoT Core (helps debug)
###########################################################################################

#resource "aws_iot_logging_options" "logging_option" {
#  default_log_level = "WARN"
#  role_arn          = aws_iam_role.iot_role.arn
#}
